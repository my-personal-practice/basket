import { MDBCard, MDBCol, MDBIcon, MDBRow } from "mdb-react-ui-kit";

import { useEffect, useState } from "react";

export default function BasketCardA({ basketProps }) {
   console.log(basketProps);
   const [active, setActive] = useState(false);

   const handleCheckboxChange = (event) => {
      setActive(event.target.checked);
      console.log(active);
   };

   return (
      <MDBRow
         style={active ? { backgroundColor: "rgb(144, 252, 216)" } : {}}
         tag="form"
         className="basketItems px-2 my-3  p-0"
      >
         <input
            checked={active}
            onChange={handleCheckboxChange}
            type="checkbox"
            id={`1`}
            className="checkbox"
         />
         <label htmlFor={`1`} className="d-flex">
            <MDBCol className="p-0 d-flex align-items-center" md={9}>
               <span className="p-0">{`name`}</span>
            </MDBCol>
            <MDBCol className="p-0 d-flex align-items-center" md={3}>
               <span className="p-0">Qty: {`quantity`}</span>
            </MDBCol>

            <MDBCol className="p-0 d-flex align-items-center" md={1}>
               <MDBIcon fas icon="times" />
            </MDBCol>
         </label>
      </MDBRow>
   );
}
